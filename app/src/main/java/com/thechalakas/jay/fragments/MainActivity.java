package com.thechalakas.jay.fragments;

/*
 * Created by jay on 16/09/17. 2:47 AM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements Fragment1.OnFragmentInteractionListener,Fragment2.OnFragmentInteractionListener
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get the fragment things
        //      FragmentManager fragmentManager = getFragmentManager();
        //FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();



        //load a fragment

        //final Fragment1 fragment1 = new Fragment1();
        //final Fragment2 fragment2 = new Fragment2();
        //fragmentTransaction.replace(android.R.id.content, ls_fragment);
        //fragmentTransaction.replace(R.id.fragment1,fragment1);

        //get the buttons
        Button buttonFragment1 = (Button) findViewById(R.id.button1);
        Button buttonFragment2 = (Button) findViewById(R.id.button2);

        //load up the first fragment
        buttonFragment1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                Log.i("MainActivity","buttonFragment1 setOnClickListener");
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //Fragment1 fragment1 = new Fragment1();
                //fragmentTransaction.replace(android.R.id.content, ls_fragment);
                //load up first fragment
                Fragment1 fragmentz = new Fragment1();
                fragmentTransaction.replace(R.id.fragment1,fragmentz);
                fragmentTransaction.commit();
            }
        });
        //load up the second fragment
        buttonFragment2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                Log.i("MainActivity","buttonFragment2 setOnClickListener");
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //load up second fragment
                Fragment2 fragmenty = new Fragment2();
                fragmentTransaction.replace(R.id.fragment1,fragmenty);
                fragmentTransaction.commit();
            }
        });



    }//end of oncreate


    @Override
    public void onFragmentInteraction(Uri uri)
    {
        //do nothing for now
    }
}
